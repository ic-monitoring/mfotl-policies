This repo is deprecated. Please instead refer to https://gitlab.com/ic-monitoring/es-log-processor 

Repository Structure
--------------------
- Each policy is stored in a subdirectory named as the corresponding Jira ticket. 
- Each policy consists of: 
	* Signature file (describing the logged events/predicates)
	* MFOTL formula file (describing traces that *violate* the policy)
	* Makefile that allows running the policy
	* At least one test log

Automatically Checking the Policies
-----------------------------------
Install the monpoly Docker image via `docker run -it infsec/monpoly:latest`.
For details, see https://hub.docker.com/r/infsec/monpoly


To run a particular policy, simply go to its subdirectory and execute `make`. For example: 
	
	$ cd VER-1348
	$ make

If the policy is accepted by the tool, the output will contain:
- The sequence of free variables, e.g. `(component,warnings,events)`.
- The (possibly, empty) list of counterexamples, e.g. `@70. (time point 5): ("nns",1,1)`
	* `70` is the (absolute) time stamp of an event that violates the policy
	* `5` is the (relative) index of the event that violates the policy
	* `("nns",1,1)` is an assignment of the free variables that violates the policy. 

Further Reading
---------------
- https://easychair.org/publications/open/62MC
- https://bitbucket.org/jshs/monpoly
