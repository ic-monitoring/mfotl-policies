MAKEFLAGS += --always-make

MONPOLY_VERSION := 1.4.1
SIGNATURES := predicates.sig

# For colorful output
RED := \033[0;31m
GREEN := \033[0;32m
YELLOW := \033[1;33m
DEFCOLOR := \033[0m

# $(call POLICY_MONITOR [POLICY_NAME] [INPUT_LOG])
define POLICY_MONITOR
	@echo "--- Policy: $(1)"
	@echo "--- Input Log: $(2)"
	docker run -it -v `pwd`:/work infsec/monpoly:$(MONPOLY_VERSION) -formula $(1)/$(1).mfotl -sig $(SIGNATURES) -log $(1)/$(2) -check
	@echo "--- Checking if $(2) complies with $(1) ..."
	docker run -it -v `pwd`:/work infsec/monpoly:$(MONPOLY_VERSION) -nofilteremptytp -formula $(1)/$(1).mfotl -sig $(SIGNATURES) -log $(1)/$(2)
	@echo "\ \ \ Policy check completed."
endef

all: \
	dummy \
	artifact_pool_latency \
	catching_up_period \
	proposal_fairness \
	unauthorized_connections \
	reboot_count \
	finalization_consistency \
	replica_divergence \
	finalized_height \
	finalized_hashes \
	clean_logs


dummy:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)


artifact_pool_latency: artifact_pool_latency.test artifact_pool_latency.real

artifact_pool_latency.test:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)

artifact_pool_latency.real:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"


catching_up_period: catching_up_period.test catching_up_period.real

catching_up_period.test:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)

catching_up_period.real:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),hourly__basic_health_pot-arshavir-zh1-spm34_zh1_dfinity_network-1644309137.node_traffic.log)


proposal_fairness: proposal_fairness.test proposal_fairness.real

proposal_fairness.test:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)

proposal_fairness.real:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),hourly__basic_health_pot-2087243880.pf.log)

unauthorized_connections: unauthorized_connections.test unauthorized_connections.real

unauthorized_connections.test:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)

unauthorized_connections.real:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),hourly__node_reassignment_pot-2075068651.uc.log)


reboot_count: reboot_count.test reboot_count.real

reboot_count.test:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),test.log)

reboot_count.real:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),hourly__basic_health_pot-arshavir-zh1-spm34_zh1_dfinity_network-1643703501.node_reboots.log)


finalization_consistency: finalization_consistency.test finalization_consistency.real

finalization_consistency.test:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"

finalization_consistency.real:
	$(eval NAME := $(basename $@))
	$(call POLICY_MONITOR,$(NAME),hourly__create_subnet-2087340405.fc.log)


replica_divergence: replica_divergence.test replica_divergence.real

replica_divergence.test:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"

replica_divergence.real:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"


finalized_height: finalized_height.test finalized_height.real

finalized_height.test:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"

finalized_height.real:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"


finalized_hashes: finalized_hashes.test finalized_hashes.real

finalized_hashes.test:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"

finalized_hashes.real:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"


clean_logs: clean_logs.test clean_logs.real

clean_logs.test:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"

clean_logs.real:
	$(eval NAME := $(basename $@))
	@echo "${YELLOW}WARNING: no log specified in $@${DEFCOLOR}"