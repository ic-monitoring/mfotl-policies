log(node_id:string, subnet_id:string, level:string, message:string)

node_added(node_id:string, subnet_id:string)
node_removed(node_id:string, subnet_id:string)

consensus_finalized(node_id:string,
                    subnet_id:string,
                    state_avail:int,
                    key_avail:int)
move_block_proposal(node_id:string,
                    subnet_id:string,
                    block_hash:string,
                    signer:string)

add_validated_BlockProposal(node:string, hash:string)
move_validated_BlockProposal(node:string, hash:string)

deliver_batch(node_id:string, subnet_id:string, block_hash:string)

ControlPlane_accept_error(local_addr:string, flow:string, err:string)
ControlPlane_accept_aborted()
ControlPlane_tls_server_handshake_failed(local_addr:string,
                                         node_id:string,
                                         peer_addr:string,
                                         error:string)

reboot(ip_addr:string, data_center_prefix:string)

finalized(node_id:string, subnet_id:string, height:int, hash:string)

replica_diverged(node_id:string, subnet_id:string, height:int)

CUP_share_proposed(node_id:string, subnet_id:string)

Exited(process:string, status:int)

end_test()